from pydantic import BaseModel


class Avatar(BaseModel):
    id: int
    image_url: str

    class Config:
        orm_mode = True


class AvatarCreate(BaseModel):
    image_url: str


class AvatarUpdate(BaseModel):
    image_url: str

