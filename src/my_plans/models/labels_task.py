from pydantic import BaseModel


class LabelTask(BaseModel):
    id: str
    label_name: str

    class Config:
        orm_mode = True


class LabelTaskCreate(BaseModel):
    label_name: str


class LabelTaskUpdate(BaseModel):
    label_name: str
