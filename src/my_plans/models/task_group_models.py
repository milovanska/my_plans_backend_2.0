from typing import List
from pydantic import BaseModel

from my_plans.models.task import Task


class TaskGroupBase(BaseModel):
    name: str


class TaskGroupCreate(TaskGroupBase):
    pass


class TaskGroupUpdate(TaskGroupBase):
    pass


class TaskGroup(TaskGroupBase):
    id: int
    tasks: List[Task]

    class Config:
        orm_mode = True
