from pydantic import BaseModel, validator
from typing import Optional
from datetime import date
from datetime import time
from enum import Enum


# __Shift type__
class TypeShifts(BaseModel):
    name: str
    id: int

    class Config:
        orm_mode = True


class CreateTypeShifts(BaseModel):
    name: str


# __Shift status__
class StatusShifts(BaseModel):
    name: str
    id: int

    class Config:
        orm_mode = True


class ActionStatusShifts(BaseModel):
    name: str


# __Shifts__
class BaseShifts(BaseModel):
    name: str
    start_at: date
    end_at: Optional[date]
    start_time: str
    end_time: str
    comment: Optional[str]
    type: str
    is_anomaly_time: Optional[bool] = False
    location: str
    isEnded: Optional[bool] = False
    status: str
    month: Optional[int] = None
    shift_user_id: int

    @validator('month', always=True)
    def validate_month(cls, _, values):
        return values.get('start_at').month


class Shifts(BaseShifts):
    id: int

    class Config:
        orm_mode = True


class CreateShifts(BaseShifts):
    pass
