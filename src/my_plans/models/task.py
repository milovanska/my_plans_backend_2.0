from datetime import datetime

from pydantic import BaseModel
from typing import List, Optional


class TaskBase(BaseModel):
    name: str
    description: str
    start_at: datetime
    finish_at: datetime
    labels: str
    reactions: str
    is_important: Optional[bool] = False
    assignee_id: Optional[int]
    reviewer_id: Optional[int]


class TaskCreate(TaskBase):
    pass


class TaskUpdate(TaskBase):
    pass


class CommentBase(BaseModel):
    text: str
    user_name: str
    user_avatar: Optional[str] = None


class CommentCreate(CommentBase):
    pass


class CommentUpdate(CommentBase):
    pass


class Comment(CommentBase):
    id: int
    created_at: datetime

    class Config:
        orm_mode = True


class Task(TaskBase):
    id: int
    comments: List[Comment]
    creator: Optional[str] = None

    class Config:
        orm_mode = True


# Модель для групп задач
class TaskGroupBase(BaseModel):
    name: str


class TaskGroupCreate(TaskGroupBase):
    pass


class TaskGroupUpdate(TaskGroupBase):
    pass


class TaskGroup(TaskGroupBase):
    id: int
    tasks: List[Task]

    class Config:
        orm_mode = True
