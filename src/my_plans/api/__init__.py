from fastapi import APIRouter

from .auth import router as auth_router
from .requests_options import router as request_options_router
from .shifts import router as shifts_router
from .task import router as task_router
from .task_group import router as task_group_router
from .avatar import router as avatar_router
from .labels_task import router as labels_task_router

router = APIRouter()
router.include_router(request_options_router)
router.include_router(auth_router)
router.include_router(shifts_router)
router.include_router(task_router)
router.include_router(task_group_router)
router.include_router(avatar_router)
router.include_router(labels_task_router)
