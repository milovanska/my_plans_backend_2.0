from typing import List

from fastapi import APIRouter
from fastapi import Depends
from fastapi import Response
from fastapi import status

from ..models.avatar import (
    AvatarCreate,
    AvatarUpdate,
    Avatar
)
from ..services.auth import get_current_user

from ..services.avatar import AvatarService
from ..tables import User

router = APIRouter(
    prefix="/avatar",
    tags=["avatar_api"]
)


@router.get("/", response_model=List[Avatar])
def get_avatars(
        user: User = Depends(get_current_user),
        service: AvatarService = Depends()
):
    return service.get_list(user_id=user.id)


@router.get('/{avatar_id}', response_model=Avatar)
def get_request_option(
    avatar_id: int,
    user: User = Depends(get_current_user),
    service: AvatarService = Depends()
):
    return service.get(user_id=user.id, avatar_id=avatar_id)


@router.post('/', response_model=Avatar)
def create_avatar(
    data: AvatarCreate,
    user: User = Depends(get_current_user),
    service: AvatarService = Depends()
):
    return service.create(user_id=user.id, data=data)


@router.put('/{avatar_id}', response_model=Avatar)
def update_avatar(
    avatar_id: int,
    avatar_data: AvatarUpdate,
    user: User = Depends(get_current_user),
    service: AvatarService = Depends()
):
    return service.update(
        user_id=user.id,
        avatar_id=avatar_id,
        avatar_data=avatar_data
    )


@router.delete('/{avatar_id}')
def delete_avatar(
    avatar_id: int,
    user: User = Depends(get_current_user),
    service: AvatarService = Depends()
):
    service.delete(user_id=user.id, avatar_id=avatar_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
