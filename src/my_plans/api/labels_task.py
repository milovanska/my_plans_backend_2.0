from typing import List

from fastapi import APIRouter
from fastapi import Depends
from fastapi import Response
from fastapi import status

from ..models.labels_task import (
    LabelTask,
    LabelTaskUpdate,
    LabelTaskCreate,
)

from ..services.auth import get_current_user
from ..services.labels_task import LabelTaskService
from ..tables import User

router = APIRouter(
    prefix="/labels_task",
    tags=["labels_task_api"],
)


@router.get("/", response_model=List[LabelTask])
def get_labels_task(
        user: User = Depends(get_current_user),
        service: LabelTaskService = Depends()
):
    return service.get_list()


@router.get('/{label_task_id}', response_model=LabelTask)
def get_label_task(
    label_task_id: int,
    user: User = Depends(get_current_user),
    service: LabelTaskService = Depends()
):
    return service.get(label_task_id=label_task_id)


@router.post('/', response_model=LabelTask)
def create_avatar(
    data: LabelTaskCreate,
    user: User = Depends(get_current_user),
    service: LabelTaskService = Depends()
):
    return service.create(data=data)


@router.put('/{label_task_id}', response_model=LabelTask)
def update_avatar(
    label_task_id: int,
    label_task_data: LabelTaskUpdate,
    user: User = Depends(get_current_user),
    service: LabelTaskService = Depends()
):
    return service.update(
        label_task_id=label_task_id,
        label_task_data=label_task_data
    )


@router.delete('/{label_task_id}')
def delete_avatar(
    label_task_id: int,
    user: User = Depends(get_current_user),
    service: LabelTaskService = Depends()
):
    service.delete(label_task_id=label_task_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)

