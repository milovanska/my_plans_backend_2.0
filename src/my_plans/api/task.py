from http.client import HTTPException
from typing import List, Optional
from fastapi import Response, status, Query
from ..models.task import Task, TaskCreate, TaskUpdate, Comment, CommentCreate, CommentUpdate
from ..services.auth import get_current_user
from ..services.task import TaskService, CommentService
from ..tables import User

from fastapi import APIRouter, Depends

router = APIRouter(
    prefix='/tasks'
)


@router.get('/', response_model=List[Task], tags=["task_api"])
def get_tasks(
    is_important: Optional[bool] = None,
    assignee_id: Optional[int] = None,
    reviewer_id: Optional[int] = None,
    user: User = Depends(get_current_user),
    service: TaskService = Depends()
):
    return service.get_list(
        user_id=user.id,
        is_important=is_important,
        assignee_id=assignee_id,
        reviewer_id=reviewer_id
    )


@router.get('/{task_id}', response_model=Task, tags=["task_api"])
def get_task(
    task_id: int,
    user: User = Depends(get_current_user),
    service: TaskService = Depends()
):
    return service.get(user_id=user.id, task_id=task_id)


@router.post("/", response_model=Task, tags=["task_api"])
def create_task(
    task_data: TaskCreate,
    user: User = Depends(get_current_user),
    task_group_id: int = Query(...),
    service: TaskService = Depends(),
):
    return service.create(user_id=user.id, task_group_id=task_group_id, data=task_data)


@router.put('/{task_id}', response_model=Task, tags=["task_api"])
def update_task(
    task_id: int,
    task_data: TaskUpdate,
    user: User = Depends(get_current_user),
    service: TaskService = Depends()
):
    return service.update(
        user_id=user.id,
        task_id=task_id,
        task_data=task_data
    )


@router.delete('/{task_id}', tags=["task_api"])
def delete_task(
    task_id: int,
    user: User = Depends(get_current_user),
    service: TaskService = Depends()
):
    service.delete(user_id=user.id, task_id=task_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.get('/{task_id}/comments', response_model=List[Comment], tags=["task_api"])
def get_comments(
    task_id: int,
    service: CommentService = Depends()
):
    return service.get_list(task_id=task_id)


@router.get('/{task_id}/comments/{comment_id}', response_model=Comment, tags=["task_api"])
def get_comment(
    task_id: int,
    comment_id: int,
    service: CommentService = Depends()
):
    return service.get(task_id=task_id, comment_id=comment_id)


@router.post('/{task_id}/comments', response_model=Comment, tags=["task_api"])
def create_comment(
    task_id: int,
    data: CommentCreate,
    service: CommentService = Depends()
):
    return service.create(task_id=task_id, data=data)


@router.put('/{task_id}/comments/{comment_id}', response_model=Comment, tags=["task_api"])
def update_comment(
    task_id: int,
    comment_id: int,
    comment_data: CommentUpdate,
    service: CommentService = Depends()
):
    return service.update(
        task_id=task_id,
        comment_id=comment_id,
        comment_data=comment_data
    )


@router.delete('/{task_id}/comments/{comment_id}', tags=["task_api"])
def delete_comment(
    task_id: int,
    comment_id: int,
    service: CommentService = Depends()
):
    service.delete(task_id=task_id, comment_id=comment_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)

