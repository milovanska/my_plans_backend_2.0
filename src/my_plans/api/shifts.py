from typing import List

from fastapi import APIRouter
from fastapi import Depends
from fastapi import Response
from fastapi import status
from fastapi import Query

from ..models.shifts import (Shifts, CreateShifts, BaseShifts, TypeShifts, CreateTypeShifts, ActionStatusShifts, StatusShifts)
from ..services.auth import get_current_user

from ..tables import User
from ..services.shifts import ShiftsService

router = APIRouter(
    prefix='/shifts'
)


@router.post('/', response_model=Shifts, tags=["shifts_api"])
def create_shifts(
        data: CreateShifts,
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    return service.create_shift(user_id=user.id, data=data)


@router.get('/', response_model=List[Shifts], tags=["shifts_api"])
def get_all_shifts(
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    return service.get_all_shifts()


@router.get('/shifts-by-month/{month}', response_model=List[Shifts], tags=["shifts_api"])
def get_shifts_by_month(
        month: int,
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    return service.get_shifts_by_month(month)


@router.put('/{shift_id}', response_model=Shifts, tags=["shifts_api"])
def update_shifts(
        shift_id: int,
        request_shift_data: BaseShifts,
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    return service.update_shifts(
        shift_id=shift_id,
        request_shift_data=request_shift_data
    )


@router.delete('/{shift_id}', tags=["shifts_api"])
def delete_shift(
        shift_id: int,
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    service.delete_shift(shift_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.get('/shift', response_model=Shifts, tags=["shifts_api"])
def get_current_shift_by_id(
        shift_id: int = Query(None, id=1),
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    return service.get_current_shift_by_id(shift_id)


# __Shift type__
@router.post('/shift-type', response_model=TypeShifts, tags=["shifts_type_api"])
def create_shift_type(
        data: CreateTypeShifts,
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    return service.create_type_shift(user_id=user.id, data=data)


@router.get('/shift-type-id', response_model=TypeShifts, tags=["shifts_type_api"])
def get_current_shift_type_by_id(
        shift_type_id: int = Query(None, id=0),
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    return service.get_shift_type_by_id(shift_type_id)


@router.get('/shift-type', response_model=List[TypeShifts], tags=["shifts_type_api"])
def get_all_shift_types(
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    return service.get_all_type_shifts()


@router.put('/shift-type/{shift_type_id}', response_model=TypeShifts, tags=["shifts_type_api"])
def update_shift_type(
        shift_type_id: int,
        request_type_shift_data: CreateTypeShifts,
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    return service.update_type_shift(
        shift_type_id=shift_type_id,
        request_type_shift_data=request_type_shift_data
    )


@router.delete('/shift-type/', tags=["shifts_type_api"])
def delete_type_shift(
        type_shift_id: int = Query(None, id=0),
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    service.delete_type_shift(type_shift_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)


# __Shift status__
@router.post('/shift-status', response_model=StatusShifts, tags=["shifts_status_api"])
def create_shift_status(
        data: ActionStatusShifts,
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    return service.create_status_shift(user_id=user.id, data=data)


@router.get('/shift-status', response_model=List[StatusShifts], tags=["shifts_status_api"])
def get_all_shift_status(
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    return service.get_all_status_shifts()


@router.get('/shift-status-id', response_model=StatusShifts, tags=["shifts_status_api"])
def get_current_shift_status_by_id(
        shift_status_id: int = Query(None, id=1),
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    return service.get_shift_status_by_id(shift_status_id)


@router.put('/shift-status-id/{shift_status_id}', response_model=StatusShifts, tags=["shifts_status_api"])
def update_shift_status(
        shift_status_id: int,
        request_status_shift_data: ActionStatusShifts,
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    return service.update_status_shift(
        shift_status_id=shift_status_id,
        request_status_shift_data=request_status_shift_data
    )


@router.delete('/shift-status-id/', tags=["shifts_status_api"])
def delete_status_shift(
        status_shift_id: int = Query(None, id=0),
        user: User = Depends(get_current_user),
        service: ShiftsService = Depends()
):
    service.delete_status_shift(status_shift_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)

