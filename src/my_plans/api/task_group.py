# from ..models.task_group import TaskGroup, TaskGroupCreate, TaskGroupUpdate
from ..models.task_group_models import TaskGroup, TaskGroupCreate, TaskGroupUpdate
from ..services.task_group import TaskGroupService

from typing import List
from fastapi import APIRouter, Depends, Response, status
from ..services.auth import get_current_user
from ..tables import User

router = APIRouter(
    prefix="/task-groups"
)


@router.get("/", response_model=List[TaskGroup], tags=["task_groups_api"])
def get_task_groups(
    user: User = Depends(get_current_user),
    service: TaskGroupService = Depends()
):
    return service.get_list(user_id=user.id)


@router.get("/{task_group_id}", response_model=TaskGroup, tags=["task_groups_api"])
def get_task_group(
    task_group_id: int,
    user: User = Depends(get_current_user),
    service: TaskGroupService = Depends()
):
    return service.get(user_id=user.id, task_group_id=task_group_id)


@router.post("/", response_model=TaskGroup, tags=["task_groups_api"])
def create_task_group(
    data: TaskGroupCreate,
    user: User = Depends(get_current_user),
    service: TaskGroupService = Depends()
):
    return service.create(user_id=user.id, data=data)


@router.put("/{task_group_id}", response_model=TaskGroup, tags=["task_groups_api"])
def update_task_group(
    task_group_id: int,
    task_group_data: TaskGroupUpdate,
    user: User = Depends(get_current_user),
        service: TaskGroupService = Depends()
):
    return service.update(
        user_id=user.id,
        task_group_id=task_group_id,
        task_group_data=task_group_data
    )


@router.delete("/{task_group_id}", tags=["task_groups_api"])
def delete_task_group(
        task_group_id: int,
        user: User = Depends(get_current_user),
        service: TaskGroupService = Depends()
):
    service.delete(user_id=user.id, task_group_id=task_group_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)

