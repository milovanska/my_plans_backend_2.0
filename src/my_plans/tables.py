from enum import unique

import sqlalchemy as sa
import sqlalchemy.orm as sa_orm
from sqlalchemy import func
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Shifts(Base):
    __tablename__ = 'shifts'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String)
    start_at = sa.Column(sa.Date)
    end_at = sa.Column(sa.Date)
    start_time = sa.Column(sa.String)
    end_time = sa.Column(sa.String)
    comment = sa.Column(sa.String)
    type = sa.Column(sa.String)
    is_anomaly_time = sa.Column(sa.Boolean)
    location = sa.Column(sa.String)
    isEnded = sa.Column(sa.Boolean)
    status = sa.Column(sa.String)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'))
    month = sa.Column(sa.Integer)
    shift_user_id = sa.Column(sa.Integer)


class TypeShifts(Base):
    __tablename__ = 'type_shifts'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'))


class StatusShifts(Base):
    __tablename__ = 'status_shifts'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'))


class RequestOptions(Base):
    __tablename__ = 'request_options'

    id = sa.Column(sa.Integer, primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'))
    request_name = sa.Column(sa.String)


class Post(Base):
    __tablename__ = 'microblog'

    id = sa.Column(sa.Integer, primary_key=True, unique=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'))
    title = sa.Column(sa.String)
    text = sa.Column(sa.Text(350))
    date = sa.Column(sa.Date)


class User(Base):
    __tablename__ = 'users'

    id = sa.Column(sa.Integer, primary_key=True)
    email = sa.Column(sa.Text, unique=True)
    username = sa.Column(sa.Text, unique=True)
    is_active = sa.Column(sa.Boolean)
    is_admin = sa.Column(sa.Boolean)
    address = sa.Column(sa.Text)
    phone = sa.Column(sa.Text)
    status = sa.Column(sa.Text)
    nickname = sa.Column(sa.Text)
    bio = sa.Column(sa.Text)
    avatar = sa.Column(sa.String)
    password_hash = sa.Column(sa.Text)
    tasks = sa.orm.relationship("Task", back_populates="user", foreign_keys="Task.user_id")
    comments = sa.orm.relationship("Comment", back_populates="user")
    task_groups = sa.orm.relationship("TaskGroup", back_populates="user")


class Task(Base):
    __tablename__ = 'tasks'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String)
    description = sa.Column(sa.Text)
    start_at = sa.Column(sa.DateTime)
    finish_at = sa.Column(sa.DateTime)
    order = sa.Column(sa.Integer)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'))
    labels = sa.Column(sa.String)
    reactions = sa.Column(sa.String)

    is_important = sa.Column(sa.Boolean, default=False)
    assignee_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'))
    reviewer_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'))

    assignee = sa.orm.relationship("User", foreign_keys=[assignee_id])
    reviewer = sa.orm.relationship("User", foreign_keys=[reviewer_id])

    comments = sa.orm.relationship("Comment", back_populates="task")
    user = sa.orm.relationship("User", back_populates="tasks", foreign_keys=[user_id])

    task_group_id = sa.Column(sa.Integer, sa.ForeignKey('task_groups.id'))
    task_group = sa.orm.relationship("TaskGroup", back_populates="tasks")

    creator = sa.Column(sa.String, nullable=True)


class Comment(Base):
    __tablename__ = 'comments'

    id = sa.Column(sa.Integer, primary_key=True)
    text = sa.Column(sa.String)
    user_name = sa.Column(sa.String)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'))
    task_id = sa.Column(sa.Integer, sa.ForeignKey('tasks.id'))
    created_at = sa.Column(sa.DateTime(timezone=True), default=func.now())
    task = sa.orm.relationship("Task", back_populates="comments")
    user = sa.orm.relationship("User", back_populates="comments")
    user_avatar = sa.Column(sa.String)


class TaskGroup(Base):
    __tablename__ = 'task_groups'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'))

    tasks = sa.orm.relationship("Task", back_populates="task_group")
    user = sa.orm.relationship("User", back_populates="task_groups")


class BaseUser:
    pass


class Avatar(Base):
    __tablename__ = "avatars"

    id = sa.Column(sa.Integer, primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'))
    image_url = sa.Column(sa.String, nullable=False)


class LabelsTask(Base):
    __tablename__ = "label_tasks"

    id = sa.Column(sa.Integer, primary_key=True)
    label_name = sa.Column(sa.String, nullable=False)
