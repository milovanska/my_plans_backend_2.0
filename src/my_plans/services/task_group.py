from typing import List
from fastapi import Depends, HTTPException, status
from ..models.task_group_models import TaskGroupCreate, TaskGroupUpdate

from sqlalchemy.orm import Session
from ..database import get_session
from .. import tables


class TaskGroupService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def get_list(self, user_id: int) -> List[tables.TaskGroup]:
        task_groups = (
            self.session
            .query(tables.TaskGroup)
            .filter_by(user_id=user_id)
            .all()
        )
        return task_groups

    def get(self, user_id: int, task_group_id: int) -> tables.TaskGroup:
        task_group = (
            self.session
            .query(tables.TaskGroup)
            .filter_by(id=task_group_id, user_id=user_id)
            .first()
        )
        if not task_group:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        return task_group

    def create(self, user_id: int, data: TaskGroupCreate) -> tables.TaskGroup:
        task_group = tables.TaskGroup(**data.dict(), user_id=user_id)
        self.session.add(task_group)
        self.session.commit()
        return task_group

    def update(
        self,
        user_id: int,
        task_group_id: int,
        task_group_data: TaskGroupUpdate
    ) -> tables.TaskGroup:
        task_group = (
            self.session
            .query(tables.TaskGroup)
            .filter_by(id=task_group_id, user_id=user_id)
            .first()
        )

        for field, value in task_group_data:
            setattr(task_group, field, value)
        self.session.commit()
        return task_group

    def delete(self, user_id: int, task_group_id: int):
        task_group = (
            self.session
            .query(tables.TaskGroup)
            .filter_by(id=task_group_id, user_id=user_id)
            .first()
        )
        self.session.delete(task_group)
        self.session.commit()

