from typing import List
from fastapi import (
    Depends,
    HTTPException,
    status
)
from sqlalchemy.orm import Session
from ..models.labels_task import (
    LabelTaskCreate,
    LabelTaskUpdate
)
from ..database import get_session
from .. import tables


class LabelTaskService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def get_list(self) -> List[tables.LabelsTask]:
        labels_task = (
            self.session
            .query(tables.LabelsTask)
            .all()
        )
        return labels_task

    def get(self, label_task_id: int) -> tables.LabelsTask:
        label_task = (
            self.session
            .query(tables.LabelsTask)
            .filter_by(id=label_task_id)
            .first()
        )
        if not label_task:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Label task not found"
            )
        return label_task

    def create(self, data: LabelTaskCreate) -> tables.LabelsTask:
        label_task = tables.LabelsTask(**data.dict())
        self.session.add(label_task)
        self.session.commit()
        return label_task

    def update(
            self,
            label_task_id: int,
            label_task_data: LabelTaskUpdate
    ) -> tables.RequestOptions:
        label_task = (
            self.session
            .query(tables.LabelsTask)
            .filter_by(id=label_task_id)
            .first()
        )

        for field, value in label_task_data:
            setattr(label_task, field, value)
        self.session.commit()
        return label_task

    def delete(self, label_task_id: int):
        label_task = (
            self.session
            .query(tables.LabelsTask)
            .filter_by(id=label_task_id)
            .first()
        )
        self.session.delete(label_task)
        self.session.commit()
