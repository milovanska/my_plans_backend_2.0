from typing import List, Optional
from fastapi import Depends, HTTPException, status
from sqlalchemy.orm import Session
from ..models.task import TaskCreate, TaskUpdate, CommentCreate, CommentUpdate
from ..database import get_session
from .. import tables


# TaskService - описание сервиса для задач
class TaskService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def get_list(self, user_id: int, is_important: Optional[bool] = None, assignee_id: Optional[int] = None,
                 reviewer_id: Optional[int] = None) -> List[tables.Task]:
        query = (
            self.session
            .query(tables.Task)
            .filter_by(user_id=user_id)
        )

        if is_important is not None:
            query = query.filter_by(is_important=is_important)

        if assignee_id is not None:
            query = query.filter_by(assignee_id=assignee_id)

        if reviewer_id is not None:
            query = query.filter_by(reviewer_id=reviewer_id)

        tasks = query.all()
        return tasks

    def get(self, user_id: int, task_id: int) -> tables.Task:
        task = (
            self.session
            .query(tables.Task)
            .filter_by(id=task_id, user_id=user_id)
            .first()
        )
        if not task:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        return task

    def create(self, user_id: int, task_group_id: int, data: TaskCreate) -> tables.Task:
        data_without_creator = data.dict(exclude_unset=True, exclude={"creator"})
        task = tables.Task(
            **data_without_creator,
            user_id=user_id,
            task_group_id=task_group_id,
            creator=self.session.query(tables.User).filter_by(id=user_id).first().username
        )
        self.session.add(task)
        self.session.commit()
        return task

    def update(
            self,
            user_id: int,
            task_id: int,
            task_data: TaskUpdate
    ) -> tables.Task:
        task = (
            self.session
            .query(tables.Task)
            .filter_by(id=task_id, user_id=user_id)
            .first()
        )

        for field, value in task_data:
            setattr(task, field, value)
        self.session.commit()
        return task

    def delete(self, user_id: int, task_id: int):
        task = (
            self.session
            .query(tables.Task)
            .filter_by(id=task_id, user_id=user_id)
            .first()
        )
        self.session.delete(task)
        self.session.commit()


# CommentService - описание сервиса для комментариев
class CommentService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def get_list(self, task_id: int) -> List[tables.Comment]:
        comments = (
            self.session
            .query(tables.Comment)
            .filter_by(task_id=task_id)
            .all()
        )
        return comments

    def get(self, task_id: int, comment_id: int) -> tables.Comment:
        comment = (
            self.session
            .query(tables.Comment)
            .filter_by(id=comment_id, task_id=task_id)
            .first()
        )
        if not comment:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        return comment

    def create(self, task_id: int, data: CommentCreate) -> tables.Comment:
        comment = tables.Comment(**data.dict(), task_id=task_id)
        self.session.add(comment)
        self.session.commit()
        return comment

    def update(
        self,
        task_id: int,
        comment_id: int,
        comment_data: CommentUpdate
    ) -> tables.Comment:
        comment = (
            self.session
            .query(tables.Comment)
            .filter_by(id=comment_id, task_id=task_id)
            .first()
        )

        for field, value in comment_data:
            setattr(comment, field, value)
        self.session.commit()
        return comment

    def delete(self, task_id: int, comment_id: int):
        comment = (
            self.session
            .query(tables.Comment)
            .filter_by(id=comment_id, task_id=task_id)
            .first()
        )
        self.session.delete(comment)
        self.session.commit()

