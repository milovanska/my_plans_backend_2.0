from typing import List
from fastapi import (
    Depends,
    HTTPException,
    status
)
from ..database import get_session
from sqlalchemy.orm import Session
from sqlalchemy import extract
from ..models.shifts import CreateShifts, BaseShifts, TypeShifts, CreateTypeShifts, ActionStatusShifts
from .. import tables

from datetime import date
from datetime import datetime


class ShiftsService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def create_shift(self, user_id: int, data: CreateShifts) -> tables.Shifts:
        shifts_options = tables.Shifts(**data.dict(), user_id=user_id)
        self.session.add(shifts_options)
        self.session.commit()
        return shifts_options

    def get_all_shifts(self) -> List[tables.Shifts]:
        shifts_options = (
            self.session
            .query(tables.Shifts)
            .all()
        )
        return shifts_options

    def get_shifts_by_month(self, month: int) -> List[tables.Shifts]:
        shifts = (
            self.session
            .query(tables.Shifts)
            .filter_by(month=month)
            .all()
        )
        return shifts

    def update_shifts(self, shift_id: int, request_shift_data: BaseShifts) -> tables.Shifts:
        shift_options = (
            self.session
            .query(tables.Shifts)
            .filter_by(id=shift_id)
            .first()
        )
        for field, value in request_shift_data:
            setattr(shift_options, field, value)
        self.session.commit()
        return shift_options

    def delete_shift(self, shift_id: int):
        request_shift = (
            self.session
            .query(tables.Shifts)
            .filter_by(id=shift_id)
            .first()
        )
        self.session.delete(request_shift)
        self.session.commit()

    def get_current_shift_by_id(self, shift_id: int) -> tables.Shifts:
        shift = (
            self.session
            .query(tables.Shifts)
            .filter_by(id=shift_id)
            .first()
        )
        return shift

    # __Shift type__
    def create_type_shift(self, user_id: int, data: CreateTypeShifts) -> tables.TypeShifts:
        type_shifts_options = tables.TypeShifts(**data.dict(), user_id=user_id)
        self.session.add(type_shifts_options)
        self.session.commit()
        return type_shifts_options

    def get_all_type_shifts(self) -> List[tables.TypeShifts]:
        request_options = (
            self.session
            .query(tables.TypeShifts)
            .all()
        )
        return request_options

    def get_shift_type_by_id(self, shift_type_id: int) -> tables.TypeShifts:
        type_shift = (
            self.session
            .query(tables.TypeShifts)
            .filter_by(id=shift_type_id)
            .first()
        )
        return type_shift

    def update_type_shift(self, shift_type_id: int, request_type_shift_data: CreateTypeShifts) -> tables.TypeShifts:
        shift_type_options = (
            self.session
            .query(tables.TypeShifts)
            .filter_by(id=shift_type_id)
            .first()
        )
        for field, value in request_type_shift_data:
            setattr(shift_type_options, field, value)
        self.session.commit()
        return shift_type_options

    def delete_type_shift(self, type_shift_id: int):
        request_type_shift = (
            self.session
            .query(tables.TypeShifts)
            .filter_by(id=type_shift_id)
            .first()
        )
        self.session.delete(request_type_shift)
        self.session.commit()

    # __Shift status__

    def create_status_shift(self, user_id: int, data: ActionStatusShifts) -> tables.StatusShifts:
        status_shifts_options = tables.StatusShifts(**data.dict(), user_id=user_id)
        self.session.add(status_shifts_options)
        self.session.commit()
        return status_shifts_options

    def get_all_status_shifts(self) -> List[tables.StatusShifts]:
        request_options = (
            self.session
            .query(tables.StatusShifts)
            .all()
        )
        return request_options

    def get_shift_status_by_id(self, shift_status_id: int) -> tables.StatusShifts:
        status_shift = (
            self.session
            .query(tables.StatusShifts)
            .filter_by(id=shift_status_id)
            .first()
        )
        return status_shift

    def update_status_shift(self, shift_status_id: int,
                            request_status_shift_data: ActionStatusShifts) -> tables.StatusShifts:
        shift_status_options = (
            self.session
            .query(tables.StatusShifts)
            .filter_by(id=shift_status_id)
            .first()
        )
        for field, value in request_status_shift_data:
            setattr(shift_status_options, field, value)
        self.session.commit()
        return shift_status_options

    def delete_status_shift(self, status_shift_id: int):
        request_status_shift = (
            self.session
            .query(tables.StatusShifts)
            .filter_by(id=status_shift_id)
            .first()
        )
        self.session.delete(request_status_shift)
        self.session.commit()
