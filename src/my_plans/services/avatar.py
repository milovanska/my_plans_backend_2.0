from typing import List

from fastapi import (
    Depends,
    HTTPException,
    status
)
from sqlalchemy.orm import Session

from ..models.avatar import (
    AvatarCreate,
    AvatarUpdate,
)
from ..database import get_session

from .. import tables


class AvatarService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def get_list(self, user_id: int) -> List[tables.Avatar]:
        avatars = (
            self.session
            .query(tables.Avatar)
            .filter_by(user_id=user_id)
            .all()
        )
        return avatars

    def get(self, user_id: int, avatar_id: int) -> tables.Avatar:
        avatar = (
            self.session
            .query(tables.Avatar)
            .filter_by(id=avatar_id, user_id=user_id)
            .first()
        )
        if not avatar:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Avatar not found"
            )
        return avatar

    def create(self, user_id: int, data: AvatarCreate) -> tables.Avatar:
        avatar = tables.Avatar(**data.dict(), user_id=user_id)
        self.session.add(avatar)
        self.session.commit()
        return avatar

    def update(
            self,
            user_id: int,
            avatar_id: int,
            avatar_data: AvatarUpdate
    ) -> tables.RequestOptions:
        avatar = (
            self.session
            .query(tables.Avatar)
            .filter_by(id=avatar_id, user_id=user_id)
            .first()
        )

        for field, value in avatar_data:
            setattr(avatar, field, value)
        self.session.commit()
        return avatar

    def delete(self, user_id: int, avatar_id: int):
        avatar = (
            self.session
            .query(tables.Avatar)
            .filter_by(id=avatar_id, user_id=user_id)
            .first()
        )
        self.session.delete(avatar)
        self.session.commit()

